#! /bin/bash -x
<< COMMENTOUT
 　<ここに説明文を書く>
COMMENTOUT

##初期処理
#引数で値がなかった場合の初期値設定
INIT_NICNAME=ens160
INIT_SLEEPTIME=300      #sec
INIT_DELAYTIME=10000ms  #ms

#引数が空なら初期値を変数に入れる。
NICNAME=${1:-$INIT_NICNAME}
SLEEPTIME=${2:-$INIT_SLEEPTIME}
DELAYTIME=${3:-$INIT_DELAYTIME}

##ここからテストシナリオ
date
echo "Start scenario!\n"

# NICのResponseを1秒遅延させる
date
tc qdisc add dev $NICNAME root netem delay $DELAYTIME

# 待つ
date
sleep $SLEEPTIME


# NICの遅延設定を元に戻す
date
tc qdisc del dev $NICNAME root

#テストシナリオ終了
date
echo "End scenario\n"
##ここまでテストシナリオ