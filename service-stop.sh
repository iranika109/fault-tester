#! /bin/bash -x
<< COMMENTOUT
 　サービス停止障害を発生させるためのスクリプト
  下記のように使います。
    # service-stop.sh <service name> <stop time(ms)>
    ex) service-stop.sh httpd 3000
COMMENTOUT

#引数で値がなかった場合の初期値設定
INIT_SERVICE=httpd
INIT_SLEEPTIME=300

#引数が空なら初期値を変数に入れる。
SERVICE=${1:-$INIT_SERVICE}
SLEEPTIME=${1:-$INIT_SLEEPTIME}

##>ここからテストシナリオ
# サービスを停止する
systemctl stop $SERVICE

# 待つ（停止を継続させる）
sleep $SLEEPTIME

# サービスを起動する
systemctl start $SERVICE
##<テストシナリオここまで
