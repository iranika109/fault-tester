#! /bin/bash
INIT_PORT=80
INIT_SLEEP=1

PORT=${2:-$INIT_PORT}
SLEEP=${3:-$INIT_SLEEP}

while read line
do
    HOST=$line
    echo "----$HOST----"
    (sleep $SLEEP;echo quit) | telnet $HOST $PORT
done < $1
