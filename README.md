# はじめに
監視設計・検証していたら監視テストの自動化したくなってきたので作ることにしました。  
障害を発生させるための検証サーバを監視システム（Zabbix等）で監視させて、  
検証サーバでテストシナリオ（sh,py,etc..）を実行して、ちゃんと障害検知ができるか確認するような感じです。

ゆくゆくは、テストシナリオの実行ログ（時間）とZabbixのイベントログ（障害）をまとめて、 
レポーティングくらいまでの仕組みを作りたいですね。  
基本的にはZabbixのテンプレート検証を想定していますが、Nagiosとかでもテストシナリオ流用できると思います。

# 想定している検証構成
\# いい感じの構成ができたら、そのうち書きます。

# How To Use
## service-stop.sh
```
サービス停止障害を起こすためのスクリプトです。  
ファイル内の「INIT_」から始まる変数の初期値を修正して実行するか、
下記のように引数で「サービス名」と「停止時間」を渡して実行します。
Ex) #./service-stop.sh <ServiceName> <StopTime(sec)>
```

## traffice-delay.sh
```
トラフィックの遅延障害を起こすためのスクリプトです。  
ファイル内の「INIT_」から始まる変数の初期値を修正して実行するか、
下記のように引数で値を渡して実行します。
Ex) #./traffice-delay.sh <NICName> <StopTIme(sec)> <DelayTime(msec)>
```
## change-file.sh
```
指定ファイルの末尾に指定文字を一時的に挿入するスクリプト。
Ex) #./change-file.sh <FileName> <SleepTime> <AddText>
```

## get-access.sh
```
指定ポートへのアクセス数をIP別に計測するスクリプト。
Ex) #./get-access.sh <:PortNum>
```

## ftelnet.sh
```
指定ポートへTCP接続を試みるスクリプト。
引数で渡されたファイルに記載のIPすべてに実行する.
Ex) #./ftelnet.sh <FilePath> <PortNum> <TimeOut>
```
