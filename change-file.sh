#! /bin/bash -x
<< COMMENTOUT
 　<ここに説明文を書く>
COMMENTOUT

#引数で値がなかった場合の初期値設定
INIT_FILE=/var/www/html/index.html
INIT_SLEEPTIME=300
INIT_ADDTEXT=hoge

#引数が空なら初期値を変数に入れる。
FILE=${1:-$INIT_FILE}
SLEEPTIME=${2:-$INIT_SLEEPTIME}
ADDTEXT=${3:-$INIT_ADDTEXT}


##/テストシナリオ
date
echo "Start scenario..."

#ファイルの末尾にADDTEXTを追記
date
echo $ADDTEXT >> $FILE

#待つ
date
sleep $SLEEPTIME

#ファイルの末尾からADDTEXTを抜く
date
sed -e "s/$ADDTEXT//g" $FILE 

date
echo "...End scenario!!"
##テストシナリオ/
