#! /bin/bash
<< COMMENTOUT
 　<ここに説明文を書く>
COMMENTOUT

#引数で値がなかった場合の初期値設定
INIT_PORT=":80\|:443"

#引数が空なら初期値を変数に入れる。
PORT=${1:-$INIT_PORT}

##/テストシナリオ
netstat -anp |grep $PORT |tee /dev/stderr| awk '{print $4}' | sort | uniq -c

##テストシナリオ/
