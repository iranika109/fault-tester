#! /bin/bash -x
<< COMMENTOUT
   repeat-script.sh <run script> <repeat num>
 　<ここに説明文を書く>
COMMENTOUT

##引数関連の処理のテンプレ
#引数で値がなかった場合の初期値設定
INIT_SCRIPT=service-stop.sh
INIT_LOOP=1
INIT_SLEEP=0

#引数が空なら初期値を変数に入れる。
SCRIPT=${1:-$INIT_SCRIPT}
LOOP=${2:-$INIT_LOOP}
SLEEP=${3:-$INIT_SLEEP}

##/テストシナリオ
echo "Start scenario..."

while [ $LOOP -gt 0 ]
do
    $SCRIPT
    LOOP=`expr $LOOP - 1`
    sleep $SLEEP
done
echo "...End scenario!!"
##テストシナリオ/
